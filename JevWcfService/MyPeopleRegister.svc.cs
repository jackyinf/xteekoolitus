﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Producer.pplService;
using WCFExtensions.Helpers.Dispatcher;

namespace Producer
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MyPeopleRegister" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select MyPeopleRegister.svc or MyPeopleRegister.svc.cs at the Solution Explorer and start debugging.
    [DispatchByBodyElementBehavior]
    public class MyPeopleRegister : pplService.PersonRegisterPortType
    {
        [DispatchBodyElement("killPerson", "http://persons_register.x-road.ee")]
        public killPersonResponse killPerson(killPersonRequest request)
        {
            var random = new Random();
            return new killPersonResponse
            {
                client = request.client,
                service = request.service,
                id = request.id,
                protocolVersion = request.protocolVersion,
                userId = request.userId,
                isKilled = random.Next(2) == 0 ? "BAM! He's dead!" : "Tried to kill him but he is still alive."
            };
        }
        
        [DispatchBodyElement("personList", "http://persons_register.x-road.ee")]
        public personListResponse personList(personListRequest request)
        {
            var juhanTamm = new person
            {
                birthDate = DateTime.Now,
                firstName = "Juhan",
                lastName = "Tamm",
                personContact = new contact
                {
                    address = "Metsa 12",
                    email = new[]{ "juhan@ee.ee" },
                    phone = new[]{ "333333" }
                }
            };
            person[] people = { juhanTamm };
            return new personListResponse
            {
                client = request.client,
                service = request.service,
                id = request.id,
                protocolVersion = request.protocolVersion,
                userId = request.userId,
                person = people
            };
        }
    }
}
