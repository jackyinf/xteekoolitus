﻿using Consumer.pplService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Consumer
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create header objects
            var protocolVersion = "4.0";
            var id = Guid.NewGuid().ToString();
            var userId = "EE39101190217";

            var service = new XRoadServiceIdentifierType
            {
                xRoadInstance = "ee-dev",
                memberClass = "COM",
                memberCode = "11333578",
                subsystemCode = "DEVTRAINING_04_A",
                serviceCode = "personList",
                serviceVersion = "v1",
                objectType = XRoadObjectType.SERVICE
            };

            var client = new XRoadClientIdentifierType
            {
                xRoadInstance = "ee-dev",
                memberClass = "COM",
                memberCode = "11333578",
                subsystemCode = "DEVTRAINING_04_B",
                objectType = XRoadObjectType.SUBSYSTEM
            };
            
            var request = new personListRequest
            {
                firstName = "1",
                lastName = "1",
                id = id,
                client = client,
                protocolVersion = protocolVersion,
                service = service,
                userId = userId
            };

           
            //Create client object
            var serviceClient = new PersonRegisterPortTypeClient();

            //var response = serviceClient.personList(request);
            var response = serviceClient.personList(request);

            Console.WriteLine(response.person[0].firstName + " " + response.person[0].lastName);
            Console.ReadKey();
        }
    }
}
